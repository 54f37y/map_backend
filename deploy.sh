rsync -azP src package.json node_modules deploy safety-dev:site/
scp .prod.env safety-dev:site/.env
echo Restarting server.
ssh safety-dev "sudo systemctl restart map; sudo service nginx reload"