# map_backend

The hardworking part of map.safe7y.com.

## Deployment

Detailed deployment instructions in [deploy.md](./deploy/deploy.md)

## Local development

1. Copy `example.env` to `.env` and fill in the missing values.
2. `npm install`
3. `npm run serve`

If you make changes that need to be reflected in the database (new columns, for instance):

`npm run sync`

Note that there are no migrations, and no backups. If you remove a column it will simply be dropped from the relevant table.