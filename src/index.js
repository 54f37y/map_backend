require('dotenv').config();

const db = require('./db');
const controller = require('./controllers');
const express = require('express');
const cors = require('cors');
const app = express();
const path = require('path');
const auth = require('./auth');
app.use(express.json());
app.use(cors());

function crud(path, controller) {
    app.get(`${path}/:id`, controller.get);
    app.post(`${path}/`, auth, controller.create);
    app.put(`${path}/`, auth, controller.update);
    controller.delete && app.delete(`${path}/:id`, auth, controller.delete);
}

crud('/api/feature', controller.feature);
app.get('/api/tile/feature/:z/:x/:y.pbf', controller.feature.getTile);

crud('/api/flag', controller.flag);
app.get('/api/tile/flag/:z/:x/:y.pbf', controller.flag.getTile);

crud('/api/object', controller.object);
app.get('/api/tile/object/:z/:x/:y.pbf', controller.object.getTile);

// app.use('/', express.static('public'));
app.use('/', express.static(path.join(__dirname, 'public')));

app.use(function (err, req, res, next) {
    console.error(err.stack);
    res.status(500).send({ message: err.message || 'Something went wrong.' });
});

const port = process.env.PORT;
app.listen(port, '0.0.0.0', async () => {
    console.log(`App listening at http://localhost:${port}`);
});
