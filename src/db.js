const { Sequelize, Model, DataTypes, QueryTypes } = require('sequelize');

require('dotenv').config();
const db = new Sequelize(
    process.env.DB_DATABASE,
    process.env.DB_USERNAME,
    process.env.DB_PASSWORD,
    {
        host: process.env.DB_HOST,
        dialect: 'postgres',
    }
);

async function rawQuery(query, options = {}) {
    return db.query(query, {
        raw: true,
        type: QueryTypes.SELECT,
        ...options,
    });
}

db.rawQuery = rawQuery; // this might be a bad idea...

module.exports = db;
