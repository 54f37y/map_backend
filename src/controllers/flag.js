const Flag = require('../models/flag.model');
const User = require('../models/user.model');
const db = require('../db');
function userId(req) {
    return req.header('userid');
}

class FlagController {
    // not actually used, flags come through feature
    static async get(req, res) {
        const payload = await Flag.findOne({
            where: { id: req.params.id },
            attributes: {
                include: [
                    [
                        db.literal(
                            `(
                SELECT "nickname"
                FROM "Users"
                WHERE "Users"."userId" = COALESCE("Flag"."updatedBy", "Flag"."createdBy")
                )`
                        ),
                        'updatedByNickname',
                    ],
                ],
            },
        });
        if (!payload) {
            res.status(404).send({ error: 'Not found.' });
        } else {
            res.send(payload.toJSON());
        }
    }
    static async create(req, res) {
        const payload = await Flag.create({
            ...req.body,
            createdBy: userId(req),
        });
        res.send(payload.toJSON());
    }

    static async update(req, res) {
        const payload = await Flag.update(
            { ...req.body, updatedBy: userId(req) },
            {
                where: {
                    id: req.body.id,
                },
            }
        );
        if (payload[0] === 1) {
            res.send({ result: 'success' });
        } else {
            res.send({ result: 'fail' });
        }
    }
    static async delete(req, res) {
        await User.updateFromRequest(req);
        await Flag.update(
            {
                deletedBy: req.header('userid'),
            },
            {
                where: { id: req.params.id },
                silent: true,
            }
        );
        const payload = await Flag.destroy({
            where: { id: req.params.id },
        });
        if (payload === 1) {
            res.send({ result: 'success' });
        } else {
            res.status(400).send({ result: 'fail' });
        }
    }

    static async getTile(req, res) {
        const { x, y, z } = req.params;
        const cols = '"flagType","subtype","id"';
        const tableName = 'Flags';
        const tileQuery = `WITH mvtgeom AS
        (
            SELECT ST_AsMVTGeom(ST_Transform(geometry, 3857), ST_TileEnvelope(${+z}, ${+x}, ${+y})) AS geom, ${cols}
            FROM "${tableName}"
            WHERE ST_Intersects(ST_Transform(geometry, 3857), ST_TileEnvelope(${+z}, ${+x}, ${+y})) AND "deletedAt" IS NULL
        )
        SELECT ST_AsMVT(mvtgeom.*)
        FROM mvtgeom`;
        const tile = (await db.rawQuery(tileQuery, { logging: false }))[0]
            .st_asmvt;
        res.write(tile, 'binary');
        res.end(null, 'binary');
    }
}
module.exports = FlagController;
