const db = require('../db');
const Object = require('../models/object.model');
const User = require('../models/user.model');
function userId(req) {
    return req.header('userid');
}
class ObjectController {
    static async get(req, res) {
        const payload = await Object.findOne({
            where: { mapillaryObjectKey: req.params.id },
        });
        if (!payload) {
            res.status(404).send({ error: 'Not found.' });
        } else {
            res.send(payload.toJSON());
        }
    }

    static async create(req, res) {
        await User.updateFromRequest(req);
        const payload = await Object.create({
            ...req.body,
            createdBy: userId(req),
        });
        res.send(payload.toJSON());
    }

    static async update(req, res) {
        await User.updateFromRequest(req);
        const payload = await Object.upsert(
            {
                ...req.body,
                updatedBy: userId(req),
            },
            {
                where: {
                    mapillaryObjectKey: req.body.mapillaryObjectKey,
                },
            }
        );
        console.log(payload);
        if (payload.length === 2) {
            res.send({ result: 'success' });
        } else {
            res.status(400).send({ result: 'fail' });
        }
    }
    // used for un-discarding
    static async delete(req, res) {
        await User.updateFromRequest(req);
        // await Object.update(
        //     {
        //         deletedBy: req.header('userid'),
        //     },
        //     {
        //         where: { id: req.params.id },
        //         silent: true,
        //     }
        // );
        const payload = await Object.destroy({
            where: { mapillaryObjectKey: req.params.id },
        });
        if (payload === 1) {
            res.send({ result: 'success' });
        } else {
            res.status(400).send({ result: 'fail' });
        }
    }

    static async getTile(req, res) {
        const { x, y, z } = req.params;
        const cols = '"mapillaryObjectKey","status","id"';
        // TODO support ?status=discard  --AND "deletedAt" IS NULL
        const tableName = 'Objects';
        // TODO save geometry in 3857 for performance
        const tileQuery = `WITH mvtgeom AS
        (
            SELECT ST_AsMVTGeom(ST_Transform(geometry, 3857), ST_TileEnvelope(${+z}, ${+x}, ${+y})) AS geom, ${cols}
            FROM "${tableName}"
            WHERE ST_Intersects(ST_Transform(geometry, 3857), ST_TileEnvelope(${+z}, ${+x}, ${+y}))
        )
        SELECT ST_AsMVT(mvtgeom.*)
        FROM mvtgeom`;
        const tile = (await db.rawQuery(tileQuery, { logging: false }))[0]
            .st_asmvt;
        res.write(tile, 'binary');
        res.end(null, 'binary');
    }
}
module.exports = ObjectController;
