const db = require('../db');
const Feature = require('../models/feature.model');
const Flag = require('../models/flag.model');
const User = require('../models/user.model');

function userId(req) {
    return req.header('userid');
}

function dbFields(req) {
    // in case we need to filter out or rename fields that don't match between front and backend
    return {
        ...req.body,
    };
}

class FeatureController {
    static async get(req, res) {
        async function getFeature() {
            const result = await Feature.findOne({
                where: { id: req.params.id },
                // Can't simply include flag because we want the updatedBy nickname
                // include: [Flag],
                attributes: {
                    include: [
                        [
                            db.literal(
                                `(
                    SELECT "nickname"
                    FROM "Users"
                    WHERE "Users"."userId" = COALESCE("Feature"."updatedBy", "Feature"."createdBy")
                    )`
                            ),
                            'updatedByNickname',
                        ],
                        [
                            db.literal(
                                `ST_Length("Feature".geometry::geography)`
                            ),
                            'length',
                        ],
                    ],
                },
            });
            return result;
        }
        async function getFlags() {
            const result = await db.rawQuery(
                `SELECT "Flags".*, "Users".nickname AS "updatedByNickname"
            FROM "Flags"
            LEFT JOIN "Users" ON "Flags"."createdBy" = "Users"."userId"
            WHERE "FeatureId" = $1;`,
                { bind: [req.params.id] }
            );
            return result;
        }
        const [feature, flags] = await Promise.all([getFeature(), getFlags()]);

        if (!feature) {
            res.status(404).send({ error: 'Not found.' });
        } else {
            res.send({ ...feature.toJSON(), Flags: flags });
        }
    }

    // static async create(req, res) {
    //     const escape = '$_s7$';
    //     const { name, roadType, geometry, featureType, subtype } = req.body;
    //     const geoms = JSON.stringify(geometry);
    //     const payload = await Feature.create({
    //         name,
    //         roadType,
    //         featureType,
    //         subtype,
    //         geometry: db.literal(
    //             `ST_Transform(ST_GeomFromGeoJSON(${escape}${geoms}${escape}), 3857)`
    //         ),
    //     });
    //     res.send(payload.toJSON());
    // }

    static async create(req, res) {
        await User.updateFromRequest(req);
        const payload = await Feature.create({
            ...dbFields(req),
            createdBy: userId(req),
        });
        res.send(payload.toJSON());
    }

    static async update(req, res) {
        await User.updateFromRequest(req);
        const payload = await Feature.update(
            {
                ...dbFields(req),
                updatedBy: userId(req),
            },
            {
                where: {
                    id: req.body.id,
                },
            }
        );
        if (payload[0] === 1) {
            res.send({ result: 'success' });
        } else {
            res.status(400).send({ result: 'fail' });
        }
    }

    static async delete(req, res) {
        await User.updateFromRequest(req);
        await Feature.update(
            {
                deletedBy: req.header('userid'),
            },
            {
                where: { id: req.params.id },
                silent: true,
            }
        );
        const payload = await Feature.destroy({
            where: { id: req.params.id },
        });
        if (payload === 1) {
            res.send({ result: 'success' });
        } else {
            res.status(400).send({ result: 'fail' });
        }
    }

    static async getTile(req, res) {
        const { x, y, z } = req.params;
        const cols = '"featureType","subtype","id"';
        const tableName = 'Features';
        // const tileQuery = `WITH mvtgeom AS
        // (
        //     SELECT ST_AsMVTGeom(geometry, ST_TileEnvelope(${+z}, ${+x}, ${+y})) AS geom, ${cols}
        //     FROM "${tableName}"
        //     WHERE ST_Intersects(geometry, ST_TileEnvelope(${+z}, ${+x}, ${+y}))
        // )
        // SELECT ST_AsMVT(mvtgeom.*)
        // FROM mvtgeom`;

        // TODO save geometry in 3857 for performance
        const tileQuery = `WITH mvtgeom AS
        (
            SELECT ST_AsMVTGeom(ST_Transform(geometry, 3857), ST_TileEnvelope(${+z}, ${+x}, ${+y})) AS geom, ${cols}
            FROM "${tableName}"
            WHERE ST_Intersects(ST_Transform(geometry, 3857), ST_TileEnvelope(${+z}, ${+x}, ${+y})) AND "deletedAt" IS NULL
        )
        SELECT ST_AsMVT(mvtgeom.*)
        FROM mvtgeom`;
        const tile = (await db.rawQuery(tileQuery, { logging: false }))[0]
            .st_asmvt;
        res.write(tile, 'binary');
        res.end(null, 'binary');
    }
}
module.exports = FeatureController;
