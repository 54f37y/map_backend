const models = require('./models');
const db = require('./db');
(async () => {
    await db.sync({ alter: true });
    console.log('Synced.');
})();
