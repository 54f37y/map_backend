const db = require('../db');
const { Model, DataTypes, QueryTypes } = require('sequelize');
const Feature = db.define(
    'Feature',
    {
        geometry: {
            type: DataTypes.GEOMETRY,
            allowNull: false,
        },
        featureType: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        subtype: DataTypes.STRING,
        device: DataTypes.STRING,
        direction: DataTypes.STRING,
        roadType: DataTypes.STRING,
        roadNumber: DataTypes.STRING,
        name: DataTypes.STRING,
        streetName: DataTypes.STRING,
        source: DataTypes.STRING,
        wikiUrl: DataTypes.STRING,
        notes: DataTypes.STRING,
        featureName: DataTypes.STRING,
        featureDescription: DataTypes.STRING,
        createdBy: DataTypes.STRING,
        updatedBy: DataTypes.STRING,
        deletedBy: DataTypes.STRING,
        crashDate: DataTypes.DATE,
        totalFatalities: DataTypes.INTEGER,
        mapillaryObjectKey: DataTypes.STRING,
        mapillaryImageKey: DataTypes.STRING,
    },
    {
        paranoid: true,
        indexes: [
            {
                fields: ['geometry'],
                using: 'gist',
            },
        ],
    }
);

module.exports = Feature;
