const db = require('../db');
const { Model, DataTypes, QueryTypes } = require('sequelize');
const User = db.define(
    'User',
    {
        userId: {
            type: DataTypes.STRING,
            primaryKey: true,
        },
        email: DataTypes.STRING,
        nickname: DataTypes.STRING,
    },
    {
        paranoid: true,
    }
);
User.updateFromRequest = async (req) => {
    return User.upsert({
        userId: req.header('userid'),
        email: req.header('useremail'),
        nickname: req.header('usernickname'),
    });
};

module.exports = User;
