const db = require('../db');
const Feature = require('./feature.model');
const { Model, DataTypes, QueryTypes } = require('sequelize');
const Object = db.define('Object', {
    geometry: DataTypes.GEOMETRY,
    mapillaryObjectKey: {
        type: DataTypes.STRING,
        unique: true,
        allowNull: false,
    },
    status: DataTypes.STRING,
    createdBy: DataTypes.STRING,
    updatedBy: DataTypes.STRING,
    deletedBy: DataTypes.STRING,
});
Feature.belongsTo(Object);
Object.hasMany(Feature);
module.exports = Object;
