const db = require('../db');
const { Model, DataTypes, QueryTypes } = require('sequelize');
const Feature = require('./feature.model');
const Flag = db.define(
    'Flag',
    {
        //creatinguser
        // featureId:
        flagType: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        subtype: DataTypes.STRING,
        notes: DataTypes.STRING,
        createdBy: DataTypes.STRING,
        updatedBy: DataTypes.STRING,
        deletedBy: DataTypes.STRING,
        geometry: DataTypes.GEOMETRY,
        mapillaryImageKey: DataTypes.STRING,
    },
    {
        paranoid: true,
    }
);
Flag.belongsTo(Feature);
Feature.hasMany(Flag);
module.exports = Flag;

/*
Choices of how to define flags that don't correspond to safety features.

1. Flags of certain types link to features (Damage, MisInstallation). Others (UnprotectedHazard, RoadDamage etc) don't. Those have a Geometry field instead.

2. Flags always link to a feature. UnprotectedHazard, RoadDamage etc (AnimalCarcass, VehicleDebris...) become Feature types. In the UI we can still make "flag-like" features look and feel different from physical features.

The UI experience either way will presumably look something like:
Workflow A:
- click existing feature
- click add flag
- choose type add details

Workflow B:
- click add flag
- choose type
- draw on map
- add other details

*/
