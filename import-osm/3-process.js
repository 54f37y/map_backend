const db = require('../src/db');
db.rawQuery(`
    DELETE FROM "Features"
    WHERE  "source" = 'OpenStreetMap - 2021-03-02 13:42:39.983695'
`);
db.rawQuery(`
    INSERT INTO "Features" ("featureType", "geometry", "subtype","device","source","notes", "createdAt", "updatedAt")
    SELECT FeatureType,wkb_geometry,Subtype,Device,Source,Notes,NOW(),NOW()
    FROM import_osm;
`);
