source ../.env
PARAMS="host=$DB_HOST user=$DB_USERNAME password=$DB_PASSWORD dbname=$DB_DATABASE"
echo "Loading OSM yield_signs"
# First import creates or drops/recreates the table
# -nlt GEOMETRY allows mixed geometry types
ogr2ogr -f "postgreSQL" PG:"$PARAMS" --config PG_USE_COPY YES -nln import_osm -nlt GEOMETRY -overwrite -progress srcdata/yield_signs.geojson
for dataset in stop_signs sound_barriers guardrails concrete_barriers cable_barriers; do
    echo "Loading $dataset..."
    # Subsequent imports append to it.
    ogr2ogr -f "postgreSQL" PG:"$PARAMS" -nln import_osm -update -append -progress --config PG_USE_COPY YES srcdata/${dataset}.geojson
done
