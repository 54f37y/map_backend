TODO: Complete this guide

Nginx
---

- Install Nginx
- Install Certbot 
- Create an Nginx configuration file to server the static site `cat nginx.conf > /etc/nginx/conf.d/map.conf`
- Check the syntax  `nginx -t`
- Restart the servce `sudo service nginx reload`
- Use certbot to setup security certificates