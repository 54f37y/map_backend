mkdir -p srcdata
wget -O srcdata/crashes-2015.geojson https://safe7y-map-public.s3.amazonaws.com/NHTSA_FARS-Crash-2015.geojson
wget -O srcdata/crashes-2016.geojson https://safe7y-map-public.s3.amazonaws.com/NHTSA_FARS-Crash-2016.geojson
wget -O srcdata/crashes-2017.geojson https://safe7y-map-public.s3.amazonaws.com/NHTSA_FARS-Crash-2017.geojson
wget -O srcdata/crashes-2018.geojson https://safe7y-map-public.s3.amazonaws.com/NHTSA_FARS-Crash-2018.geojson
wget -O srcdata/crashes-2019.geojson https://safe7y-map-public.s3.amazonaws.com/NHTSA_FARS-Crash-2018.geojson
