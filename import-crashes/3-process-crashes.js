const db = require('../src/db');
// there are lots of rows with long/lat of 999.99/99.99 (yes, ~1000, ~100)
db.rawQuery(`
    DELETE FROM "Features"
    WHERE "featureType" = 'Crash' AND "source" = 'NHTSA - FARS'
`);
db.rawQuery(`
    INSERT INTO "Features" ("featureType", "geometry", "roadType", "subtype","source", "wikiUrl", "crashDate", "totalFatalities", "createdAt", "updatedAt")
    SELECT featuretype,wkb_geometry,roadtype,subtype,source,wikilink,DATE(crashdate),"totalfatalities",NOW(),NOW()
    FROM import_crashes
    WHERE ST_Y(wkb_geometry) < 85 AND ST_X(wkb_geometry) < 180;
`);
