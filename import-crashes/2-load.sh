source ../.env
PARAMS="host=$DB_HOST user=$DB_USERNAME password=$DB_PASSWORD dbname=$DB_DATABASE"
echo "Loading 2015..."
ogr2ogr -f "postgreSQL" PG:"$PARAMS" --config PG_USE_COPY YES -nln import_crashes -overwrite -progress srcdata/crashes-2015.geojson
for year in 2016 2017 2018 2019; do
    echo "Loading $year..."
    ogr2ogr -f "postgreSQL" PG:"$PARAMS" -nln import_crashes -update -append -progress --config PG_USE_COPY YES srcdata/crashes-${year}.geojson
done
